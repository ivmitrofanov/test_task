<?php
use PHPUnit\Framework\TestCase;

class SequenceTest extends TestCase
{

	function runTst($sequence, $is_sequence)
	{
		system('php ./script.php ' . $sequence, $retval);
		$this->assertSame($retval, $is_sequence);
	}

	function testNotSequence()
	{
		$this->runTst('', 0);
		$this->runTst('1', 0);
		$this->runTst('1,2', 0);
		$this->runTst('1,2,5', 0);
		$this->runTst('1,2,6', 0);
		$this->runTst('1,2,4,8,16,33', 0);
		$this->runTst('1,2,4,16,32,64,128', 0);
		
		$this->runTst('5,5,5,6', 0);
		$this->runTst('5,6,5,6', 0);
		$this->runTst('1,2,4,f', 0);
	}

	function testSimpleSequence()
	{

		$this->runTst('1,2,4', 1);
		$this->runTst('1,2,4,8', 1);
		$this->runTst('5,5,5', 1);
	}

	function testLongAlgSequence()
	{

		$sequence_array = [];

		$value = 0;
		for ($i = 0; $i < 1000; $i++) {
			$value += 3.5;
			$sequence_array[$i] = $value;
		}

		$sequence = implode(',', $sequence_array);
		$this->runTst($sequence, 1);
	}

	function testWrongLongAlgSequence()
	{

		$sequence_array = [];

		$value = 0;
		for ($i = 0; $i < 1000; $i++) {
			$value += 3.5;
			$sequence_array[$i] = $value;
		}

		$sequence_array[134] = 13;
		$sequence = implode(',', $sequence_array);
		$this->runTst($sequence, 0);
	}

	function testLongGeomSequence()
	{

		$sequence_array = [];

		$value = 1;
		for ($i = 0; $i < 200; $i++) {
			$value *= 1.1;
			$sequence_array[$i] = $value;
		}

		$sequence = implode(',', $sequence_array);

		$this->runTst($sequence, 1);
	}

	function testLongWrongGeomSequence()
	{

		$sequence_array = [];

		$value = 1;
		for ($i = 0; $i < 200; $i++) {
			$value *= 1.1;
			$sequence_array[$i] = $value;
		}

		$sequence_array[134] = 23;
		$sequence = implode(',', $sequence_array);

		$this->runTst($sequence, 0);
	}

	function testLongGeomSequence2()
	{

		$sequence_array = [];

		$value = 1;
		for ($i = 0; $i < 200; $i++) {
			$value /= 1.1;
			$sequence_array[$i] = $value;
		}


		$sequence = implode(',', $sequence_array);

		$this->runTst($sequence, 1);
	}

}