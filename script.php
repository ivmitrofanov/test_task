<?
$input_line = $argv[1]; // keep in mind that there are limits for arguments lengths!
$input_length = strlen($input_line);

$current_text_value = '';
$current_value = 0;
$previous_value = 0;

$algebraic_diff = 0;
$geometric_diff = 0;

function are_floats_equal($a, $b)
{

	if (abs($a - $b) < 0.00001)
		return true;

	return false;
}

$i = 0;
$current_index = 0; //index of an element in a sequence
$is_sequence = true;

while ($i < $input_length) {

	$current_char = $input_line{$i};

	if ($current_char === ',' || $i === $input_length - 1) {

		if ($i === $input_length - 1)
			$current_text_value = $current_text_value . $current_char;

		if (is_numeric($current_text_value)) {

			$current_value = strpos($current_text_value, '.') === false ? intval($current_text_value) : floatval($current_text_value);

			switch ($current_index) {
				case 0:

					$previous_value = $current_value;
					break;

				case 1:

					$algebraic_diff = $current_value - $previous_value;
					$geometric_diff = $current_value / $previous_value;
					break;

				default:

					//more checks (for different types of sequences) are possible here
					if (!are_floats_equal($current_value / $previous_value, $geometric_diff) && !are_floats_equal($current_text_value - $previous_value, $algebraic_diff)) {

						$is_sequence = false;

						break 2; //breaking outer loop
					}

					break;
			}

			$current_text_value = '';
			$previous_value = $current_value;

		} else {

			$is_sequence = false;
			break;

		}

		$current_index++;
	} else {

		$current_text_value = $current_text_value . $current_char;

	}

	$i++;
}

if ($current_index <= 2) // if there are less than 3 elements in an input sequence, it's not a math sequence
	$is_sequence = false;

exit ((int)$is_sequence);